package com.BasicScripts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class CrossBrowser {

	public static void main(String[] args) throws Exception {
		WebDriver driver = null;
		//Enter data using BufferReader 
        BufferedReader reader =  
                   new BufferedReader(new InputStreamReader(System.in)); 
         
        // Reading data using readLine 
        System.out.println("Enter Browser Name:");
        String name = reader.readLine();
        if(name.equalsIgnoreCase("Chrome")) {
        	System.setProperty("webdriver.chrome.driver","E:\\Selenium\\Chrome driver\\32\\chromedriver.exe");
    		//Create f=driver object for chrome browser
    		 driver = new ChromeDriver();
        }
        else {
        	if(name.equalsIgnoreCase("Firefox")) {
        		System.setProperty("webdriver.gecko.driver","E:\\Selenium\\gecko\\geckodriver.exe");
        		//Create f=driver object for Mozile browser
        	 driver = new FirefoxDriver();
        		
        	}
	}
     
        	if(name.equalsIgnoreCase("IE")) {
        		System.setProperty("webdriver.ie.driver","E:\\Selenium\\ie\\MicrosoftWebDriver.exe");
        		//Create f=driver object for IE browser
        	 driver = new InternetExplorerDriver();
        		
        	}
	
        driver.get("https://seleniumhq.github.io/selenium/docs/api/java/index.html");
        Thread.sleep(2000);
        driver.close();

  }
}
