package com.BasicScripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstScript {

	public static void main(String[] args) throws Exception {
		//set property for chrome driver
		System.setProperty("webdriver.chrome.driver","E:\\Selenium\\Chrome driver\\32\\chromedriver.exe");
		//Create f=driver object for chrome browser
		WebDriver driver = new ChromeDriver();
		driver.get("https://seleniumhq.github.io/selenium/docs/api/java/index.html");
		Thread.sleep(2000);
		System.out.println("URL: "+driver.getCurrentUrl());
		System.out.println("Title: "+driver.getTitle());
		System.out.println("Source: "+driver.getPageSource());
		driver.close();

	}

}
